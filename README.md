## TabsFolders-Sikulix

[TabsFolders](https://www.tabsfolders.com) will help you Organize your bookmarks into folders & share it with your friends securely. This project is providing automation tasks for the required use cases using GUI automation tool - [Sikuli](http://www.sikuli.org/)

## Pre-requisites

1) Allow pop-ups for TabsFolders.com for all the browsers.

2) [Sikuli Setup](http://nightly.sikuli.de/)

    Install Sikuli setup with the second option. Once setup is success,
    it will generate SikuliX.app, which can be moved to Applications folder.

3) Accessibility Setup in Mac

Click on System Preferences --> Security & Privacy --> Accessibilty

    If you are using eclipse to run the tests --> add eclipse app here.
    If you are using intellijIdea to run tests --> add intelliJ Idea here.
    If you are using terminal to launch maven tests, add terminal app here.
    
4) Maven version > 3.0+ 

## Tests

First Launch terminal App and navigate to the project root directory [i.e TabsFolders-Sikuli]

To run only safari tests

    mvn clean -Dtest=SafariTest verify -X -e surefire-report:report

To run only install app test for safari

    mvn clean -Dtest=InstallTest#test1SafariInstallPlugin verify -X -e surefire-report:report

To run only chrome tests

    mvn clean -Dtest=ChromeTest verify -X -e surefire-report:report

To run only install app test for chrome

    mvn clean -Dtest=InstallTest#test1ChromeInstallPlugin verify -X -e surefire-report:report


To run only firefox tests

    mvn clean -Dtest=FirefoxTest verify -X -e surefire-report:report

To run only install app test for firefox

    mvn clean -Dtest=InstallTest#test1FirefoxInstallPlugin verify -X -e surefire-report:report

## Test Results

    Each test run will create a target folder under project root directory. With in the target folder,
    you will see surefire-reports folder and site folder. surefire-reports folder contains 
    Junit xml files and text files which will give you the total tests passed, failed Or not run.
    Site folder contains html file which will show the same results in a UI formatted report.
    
## Failure Analysis

    Each test failure will generate a screenshot under project root folder. Also the test results 
    will tell us the line number at which sikuli failed to perform an action. By using one of them or both,
    we can analyse the failure reason.

## Browsers supported

    Chrome version:44.0.2403.157 (64-bit)
    Firefox version: 40.0
    Safari Version: 8.0.8

## Note

> These scripts are developed using sikuli-1.1.0 version on Mac Os 10.10.5 with Retina Display. 
> As Sikuli is based on images, it is advisable to run against the same combinations for getting 
> correct results.
    
## Contributors

@naiduasn

## License

Open Source
