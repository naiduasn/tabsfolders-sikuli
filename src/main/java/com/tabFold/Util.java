package com.tabFold;

import org.sikuli.basics.Debug;
import org.sikuli.script.*;

/**
 * Created by sanyasi.annepu on 11/08/15.
 */
public class Util {

    public static App startBrowserOpenLink(
            String browser, String link) throws FindFailed {
        Screen screen = new Screen();
        App ff = new App(browser);
        ff.focus();
        screen.type("l", Key.CMD);
        screen.wait((double) 3.0);
        screen.type("l", Key.CMD);
        screen.paste(link);
        screen.type(Key.ENTER);
        //screen.wait(image, waitTime);

        return ff;
    }

    public static App startBrowser(
            String browser) throws FindFailed {
        Screen screen = new Screen();
        App ff = new App(browser);
        ff.focus();
        screen.wait((double) 3.0);
        return ff;
    }

    public static void click(String image) throws FindFailed {
        Screen screen = new Screen();
        screen.wait(image, 20);
        Match imgMatch = screen.getLastMatch();
        screen.click(imgMatch.getCenter());
    }

    public static void doubleClick(String image) throws FindFailed {
        Screen screen = new Screen();
        screen.wait(image, 20);
        Match imgMatch = screen.getLastMatch();
        screen.doubleClick(imgMatch.getCenter());
    }

    public static void rightClick(String image) throws FindFailed {
        Screen screen = new Screen();
        screen.wait(image, 20);
        Match imgMatch = screen.getLastMatch();
        screen.rightClick(imgMatch.getCenter());
    }

    public static void clickWithPattern(String image, float sim) throws FindFailed {
        Screen screen = new Screen();
        Pattern testPattern = new Pattern(image);
        testPattern = testPattern.similar(sim);
        screen.wait(testPattern, (double) 20.0);
        screen.click(testPattern, 0);
    }

    public static void rightClickWithPattern(String image, float sim) throws FindFailed {
        Screen screen = new Screen();
        Pattern testPattern = new Pattern(image);
        testPattern = testPattern.similar(sim);
        screen.wait(testPattern, (double) 20.0);
        screen.rightClick(testPattern, 0);
    }

    public static void clickWithPatternOffset(String image, float sim, int x, int y) throws FindFailed {
        Screen screen = new Screen();
        Pattern testPattern = new Pattern(image).targetOffset(x,y);
        testPattern = testPattern.similar(sim);
        screen.wait(testPattern, (double) 20.0);
        screen.click(testPattern, 0);
    }

    public static void clickWithOffset(String image, int x, int y) throws FindFailed {
        Screen screen = new Screen();
        Pattern testPattern = new Pattern(image).targetOffset(x,y);
        screen.wait(testPattern, (double) 20.0);
        screen.click(testPattern, 0);
    }

    public static void doubleClickWithPattern(String image, float sim) throws FindFailed {
        Screen screen = new Screen();
        Pattern testPattern = new Pattern(image);
        testPattern = testPattern.similar(sim);
        screen.wait(testPattern);
        screen.doubleClick(testPattern, 0);
    }

    public static Match findImgMatch(String image, float sim) throws FindFailed {
        Screen screen = new Screen();
        Pattern testPattern = new Pattern(image);
        testPattern = new Pattern(image);
        testPattern = testPattern.similar(sim);
        Match actualImg = screen.wait(testPattern);
        return actualImg;
    }

    public static void type(String image, String text) throws FindFailed {
        Screen screen = new Screen();
        screen.wait(image, 20);
        Match imgMatch = screen.getLastMatch();
        screen.type(imgMatch.getCenter(), text);
    }

    public static void typeWithPattern(String image, String text, float sim) throws FindFailed {
        Screen screen = new Screen();
        Pattern testPattern = new Pattern(image);
        testPattern = testPattern.similar(sim);
        screen.wait(testPattern, (double) 20.0);
        screen.type(testPattern, text);
    }

    public static Boolean isCleanComplete() {
        Screen screen = new Screen();
        Pattern pattern = new Pattern("home");
        pattern = pattern.similar(0.90f);
        try {
            screen.wait(pattern, (double) 10.0);
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
        }
        Region region = screen.getLastMatch().below(30);
//        region.highlight(5);
        region.getCenter().rightClick();
        try {
            region = screen.find("trash");
            //        region.highlight(5);
            region.getCenter().click();
            return false;
        } catch (FindFailed findFailed) {
            findFailed.printStackTrace();
            return true;
        }


    }

    public static void openTab(String link) {
        Screen screen = new Screen();
        screen.type("t", KeyModifier.CMD);
        screen.type("l", Key.CMD);
        screen.paste(link);
        screen.type(Key.ENTER);
        screen.wait((double) 5.0);
    }

    public static void createFolder(String name) throws FindFailed {
        Screen screen = new Screen();
        rightClick("home");
        click("create");
        screen.wait((double) 2.0);
        screen.type(name);
        screen.type(Key.ENTER);
        screen.wait((double) 1.0);
    }

    public static void openBookmarks(String name) throws FindFailed {
        Screen screen = new Screen();
        rightClick("news");
        screen.hover("open");
        click(name);
    }

    public static void closeTabs() throws FindFailed{
        Screen screen = new Screen();
        Pattern testPattern = new Pattern("extInstalImg");
        testPattern = testPattern.similar(0.81f);
        while (screen.exists(testPattern) != null){
            screen.type("w", Key.CMD);
            screen.wait((double) 1.0);
        }
    }

    public static void firefoxMultiTabSave() throws FindFailed{
        Screen screen = new Screen();
        screen.wait((double) 3.0);
        screen.rightClick(screen.getCenter());
        screen.wait("cntxmenu", (double) 5.0);
        screen.hover("cntxmenu");
        screen.wait("cntxmenu2",(double) 5.0);
        screen.click("cntxmenu2");
    }
}
