package com.tabFold;

import com.google.common.collect.Iterators;
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.sikuli.basics.Debug;
import org.sikuli.basics.Settings;
import org.sikuli.script.*;

import java.util.Iterator;

/**
 * Created by sanyasi.annepu on 11/08/15.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InstallTest extends BaseTest{

    @Before
    public void setup() throws FindFailed {

    }

    /**
     * @throws Exception
     */
    @Test
    public void test1SafariInstallPlugin() throws FindFailed {
        String browser = Constants.BROWSER_S;
        Screen screen = new Screen();
        String IMG_DIR = "com.tabFold.SafariTest/images.sikuli/";
        Debug.user("[STARTING] running test with %s", browser);
        Debug.on(3);
        Settings.ActionLogs = false;
        Settings.InfoLogs = false;
        ImagePath.add(IMG_DIR + browser);

        sfr = Util.startBrowserOpenLink(browser, "http://www.tabsfolders.com");
        Util.click("getTabsFolders");
        screen.wait((double) 5.0);
        Util.clickWithPattern("download", 0.90f);
        screen.wait((double) 5.0);
        Util.doubleClickWithPattern("extension", 0.81f);
        Util.clickWithPattern("install", 0.90f);

        screen.wait((double) 5.0);

        if (screen.exists("fbpwd", (double) 30.0) != null) {
            if (screen.exists("fbuname", (double) 1.0) != null) {
                Util.typeWithPattern("fbuname", Constants.FB_UNAME, 0.80f);
            }
            screen.type(Key.TAB);
            Util.typeWithPattern("fbpwd", Constants.FB_PWD, 0.80f);
            Util.clickWithPattern("fblogin", 0.80f);
        }
        if (screen.exists("endtour", (double) 10.0) != null) {
            Util.click("endtour");
        }
        Match actualImg = Util.findImgMatch("extInstalImg", 0.80f);
        Assert.assertNotNull("Not installed Tab folders extension", actualImg);

    }

    /**
     * @throws Exception
     */
    @Test
    public void test1ChromeInstallPlugin() throws FindFailed {
        String browser = Constants.BROWSER_C;
        Screen screen = new Screen();
        String IMG_DIR = "com.tabFold.ChromeTest/images.sikuli/";

        Debug.user("[STARTING] running test with %s", browser);
        Debug.on(3);
        Settings.ActionLogs = false;
        Settings.InfoLogs = false;
        ImagePath.add(IMG_DIR + "chrome");

        sfr = Util.startBrowserOpenLink(browser, "http://www.tabsfolders.com");
        Util.click("getTabsFolders");
        Util.clickWithPattern("install", 0.81f);
        Util.clickWithPattern("installNow", 0.80f);

        screen.wait((double) 5.0);
        if (screen.exists("fbpwd", (double) 30.0) != null) {
            if (screen.exists("fbuname", (double) 1.0) != null) {
                Util.typeWithPattern("fbuname", Constants.FB_UNAME, 0.80f);
            }
            screen.type(Key.TAB);
            Util.typeWithPattern("fbpwd", Constants.FB_PWD, 0.80f);
            Util.clickWithPattern("fblogin", 0.80f);
        }
        if (screen.exists("endtour", (double) 10.0) != null) {
            Util.click("endtour");
        }
        Assert.assertNotNull(screen.exists("trashhome", (double) 15.0));

    }

    /**
     * @throws Exception
     */
    @Test
    public void test1FirefoxInstallPlugin() throws FindFailed {

        String browser = Constants.BROWSER_F;
        Screen screen = new Screen();
        String IMG_DIR = "com.tabFold.FirefoxTest/images.sikuli/";

        Debug.user("[STARTING] running test with %s", browser);
        Debug.on(3);
        Settings.ActionLogs = false;
        Settings.InfoLogs = false;
        ImagePath.add(IMG_DIR + browser);

        sfr = Util.startBrowserOpenLink(browser, "http://www.tabsfolders.com");
        Util.click("getTabsFolders");
        Util.clickWithPattern("install", 0.81f);
        Util.clickWithPattern("installNow", 0.90f);

        screen.wait((double) 5.0);
        if (screen.exists("fbpwd", (double) 30.0) != null) {
            if (screen.exists("fbuname", (double) 1.0) != null) {
                Util.typeWithPattern("fbuname", Constants.FB_UNAME, 0.80f);
            }
            screen.type(Key.TAB);
            Util.typeWithPattern("fbpwd", Constants.FB_PWD, 0.80f);
            Util.clickWithPattern("fblogin", 0.80f);
        }
        if (screen.exists("endtour", (double) 10.0) != null) {
            Util.click("endtour");
        }
        Assert.assertNotNull(screen.exists("trashhome", (double) 10.0));

    }


    @After
    public void cleanup() throws FindFailed {

    }
}
