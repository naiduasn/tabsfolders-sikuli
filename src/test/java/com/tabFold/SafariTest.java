package com.tabFold;

import com.google.common.collect.Iterators;
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.sikuli.basics.Debug;
import org.sikuli.basics.Settings;
import org.sikuli.script.*;

import java.util.Iterator;

/**
 * Created by sanyasi.annepu on 11/08/15.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SafariTest extends BaseTest{
    String browser = Constants.BROWSER_S;
    Screen screen = new Screen();
    public static final String IMG_DIR = "com.tabFold.SafariTest/images.sikuli/";

    @Before
    public void setup() throws FindFailed {
        Debug.user("[STARTING] running test with %s", browser);
        Debug.on(3);
        Settings.ActionLogs = false;
        Settings.InfoLogs = false;
        ImagePath.add(IMG_DIR + browser);
    }

    /**
     * @throws Exception
     */
    @Test
    public void test3SafariCleanup() throws FindFailed {

        sfr = Util.startBrowser(browser);
        if (screen.exists("fbpwd", (double) 15.0) != null) {
            if (screen.exists("fbuname", (double) 1.0) != null) {
                Util.typeWithPattern("fbuname", Constants.FB_UNAME, 0.80f);
            }
            screen.type(Key.TAB);
            Util.typeWithPattern("fbpwd", Constants.FB_PWD, 0.80f);
            Util.clickWithPattern("fblogin", 0.80f);
        }

        if (screen.exists("endtour", (double) 5.0) != null) {
            Util.click("endtour");
        }
        if(screen.exists("norating") !=null){
            Util.click("norating");
        }
        Boolean isTrashComplete = false;
        // temporaray fix for Lost & found folder which cannot be deleted in safari
        int count = 0;
        while (!isTrashComplete && count <= 20) {
            isTrashComplete = Util.isCleanComplete();
            count++;
        }
        Assert.assertTrue("When Lost+Found folder appears in safari, it cannot be deleted.",isTrashComplete || count > 20);
    }

    @Test
    public void test4MultipleTabSave() throws FindFailed {

        sfr = Util.startBrowser(browser);
        screen.wait((double) 5.0);
        if (screen.exists("discard", (double) 5.0) != null) {
            Util.click("discard");
        }
        for (int i = 0; i < Constants.URLS.length; i++) {
            Util.openTab(Constants.URLS[i]);
        }
        screen.wait((double) 10.0);
        Util.clickWithPattern("extInstalImg", 0.81f);
        Util.clickWithOffset("home", -18, 0);
        Util.clickWithOffset("expand", -18, 0);
        screen.wait((double) 5.0);
        Assert.assertNotNull("Failed to add bookmarks using TabsFolders", screen.exists("bingicon"));
    }

    @Test
    public void test5CreateFolders() throws FindFailed {

        sfr = Util.startBrowser(browser);
        screen.wait((double) 5.0);
        if (screen.exists("discard", (double) 5.0) != null) {
            Util.click("discard");
        }
        Util.createFolder("News");
        Util.createFolder("Search");
        Util.createFolder("Sports");
        Util.createFolder("Shopping");

        Assert.assertNotNull("Failed to create folders using TabsFolders", screen.exists("news"));
        Assert.assertNotNull("Failed to create folders using TabsFolders", screen.exists("search"));
        Assert.assertNotNull("Failed to create folders using TabsFolders", screen.exists("shopping"));
        Assert.assertNotNull("Failed to create folders using TabsFolders", screen.exists("sports"));

    }

    @Test
    public void test6MoveBookmarks() throws FindFailed {

        sfr = Util.startBrowser(browser);
        screen.wait((double) 5.0);
        if (screen.exists("discard", (double) 5.0) != null) {
            Util.click("discard");
        }
        if (screen.exists("bbcicon", (double) 2.0) == null) {
            Util.clickWithOffset("expand", -18, 0);
        }
        screen.dragDrop("bbcicon", "news");
        screen.wait((double) 1.0);
        screen.dragDrop("cnnicon", "news");
        screen.wait((double) 1.0);
        screen.dragDrop("amazonicon", "shopping");
        screen.wait((double) 1.0);
        screen.dragDrop("ebayicon", "shopping");
        screen.wait((double) 1.0);
        screen.dragDrop("bingicon", "search");
        screen.wait((double) 1.0);
        screen.dragDrop("googleicon", "search");
        screen.wait((double) 1.0);
        screen.dragDrop("goalicon", "sports");
        screen.wait((double) 1.0);
        screen.dragDrop("espnicon", "sports");

        Assert.assertNull("Failed to Move bookmarks using TabsFolders", screen.exists(new Pattern("bbcicon").exact()));
        Assert.assertNull("Failed to Move bookmarks using TabsFolders", screen.exists(new Pattern("cnnicon").exact()));
        Assert.assertNull("Failed to Move bookmarks using TabsFolders", screen.exists(new Pattern("amazonicon").exact()));
        Assert.assertNull("Failed to Move bookmarks using TabsFolders", screen.exists(new Pattern("ebayicon").exact()));
        Assert.assertNull("Failed to Move bookmarks using TabsFolders", screen.exists(new Pattern("bingicon").exact()));
        Assert.assertNull("Failed to Move bookmarks using TabsFolders", screen.exists(new Pattern("googleicon").exact()));
        Assert.assertNull("Failed to Move bookmarks using TabsFolders", screen.exists(new Pattern("goalicon").exact()));
        Assert.assertNull("Failed to Move bookmarks using TabsFolders", screen.exists(new Pattern("espnicon").exact()));
    }

    @Test
    public void test7OpenBookmarks() throws FindFailed {

        sfr = Util.startBrowser(browser);
        screen.wait((double) 5.0);
        if (screen.exists("discard", (double) 5.0) != null) {
            Util.click("discard");
        }
        Boolean isOpened = false;
        Util.openBookmarks("currentWindow");
        screen.wait((double) 5.0);
        if (screen.exists("bbclogo", (double) 15.0) != null || screen.exists("cnnlogo", (double) 15.0) != null) {
            isOpened = true;
        }
        screen.type(Key.TAB, KeyModifier.CTRL);
        if (screen.exists("home", (double) 5.0) == null) {
            screen.type(Key.TAB, KeyModifier.CTRL);
            screen.wait((double) 1.0);
            isOpened = (screen.exists("home", (double) 5.0) != null);
        }
        Assert.assertTrue(isOpened);
        screen.type("q", Key.CMD);

        screen.wait((double) 5.0);
        Boolean isNew = false;
        sfr = Util.startBrowser(browser);
        screen.wait((double) 5.0);
        if (screen.exists("discard", (double) 5.0) != null) {
            Util.click("discard");
        }

        Util.openBookmarks("newwindow");
        if (screen.exists("bbclogo", (double) 25.0) != null || screen.exists("cnnlogo", (double) 25.0) != null) {
            isNew = true;
        }
        screen.type(Key.TAB, KeyModifier.CTRL);
        // In clients system, when we open book marks in new window via safari browser, it does not open in full screen mode.
        //Assert.assertNull(screen.exists("home"));
        Assert.assertTrue(isNew);
        Util.closeTabs();
    }

    @Test
    public void test8CutCopyPasteBookmarks() throws FindFailed {

        sfr = Util.startBrowser(browser);
        screen.wait((double) 5.0);
        if (screen.exists("discard", (double) 5.0) != null) {
            Util.click("discard");
        }

        Util.rightClickWithPattern("news", 0.80f);
        Util.click("copy");
        Util.rightClick("home");
        Util.click("paste");
        screen.wait((double) 2.0);
        Iterator<Match> matches = screen.findAll("news");
        int size = Iterators.size(matches);
        Debug.info("number of sports folders after copy/paste:" + size);
        Assert.assertTrue(size > 2);

        Util.rightClickWithPattern("news", 0.80f);
        Util.click("trash");
        screen.wait((double) 2.0);


        Util.rightClickWithPattern("shopping", 0.80f);
        Util.click("cut");
        Util.rightClick("home");
        Util.click("paste");
        screen.wait((double) 2.0);
        matches = screen.findAll("shopping");
        size = Iterators.size(matches);
        Debug.info("number of sports folders after cut/paste:" + size);
        Assert.assertTrue(size == 2);

        Util.clickWithPattern("news", 0.80f);
        screen.type("c", Key.CMD);
        screen.wait((double) 1.0);
        Util.rightClick("home");
        screen.wait((double) 1.0);
        Util.click("paste");
        screen.wait((double) 1.0);
        matches = screen.findAll("news");
        size = Iterators.size(matches);
        Debug.info("number of news folders after copy/paste:" + size);
        Assert.assertTrue(size > 2);
        Util.doubleClick("home");
        screen.wait((double) 1.0);

        Util.clickWithPattern("shopping", 0.80f);
        screen.type("x", KeyModifier.CMD);
        Util.rightClick("home");
        Util.click("paste");
        Util.doubleClick("home");
        screen.wait((double) 2.0);
        matches = screen.findAll("shopping");
        size = Iterators.size(matches);
        Debug.info("number of sports folders after cut/paste:" + size);
        Assert.assertTrue(size > 0);

        Util.rightClickWithPattern("news", 0.80f);
        Util.click("trash");
        screen.wait((double) 2.0);
    }

    @Test
    public void test9ShareLink() throws FindFailed {
        sfr = Util.startBrowser(browser);
        screen.wait((double) 5.0);
        if (screen.exists("discard", (double) 5.0) != null) {
            Util.click("discard");
        }

        Util.rightClickWithPattern("news", 0.80f);
        Util.clickWithPattern("share", 0.80f);
        screen.wait((double) 2.0);

        Util.click("sharefolder");
        screen.wait("emailshare", (double) 10.0);
        Util.click("emailshare");
        screen.type("enteremail", "testqaauto8@gmail.com");
        Util.clickWithPattern("send", 0.80f);
        screen.wait((double) 5.0);
        Util.click("done");

        screen.wait("sharefolderimg", (double) 10.0);
        Util.rightClick("sharefolderimg");
        Util.clickWithPattern("share", 0.80f);
        screen.wait("done", (double) 10.0);
        Util.clickWithPattern("open", 0.80f);

        Assert.assertNotNull(screen.wait("import", (double) 10.0));
        screen.type("w", Key.CMD);
        Util.click("done");

    }

    /**
     * @throws Exception
     */
    @Test
    public void testUnInstallPlugin() throws FindFailed {
        sfr = Util.startBrowser(browser);
        screen.wait((double) 3.0);
        screen.type("w", KeyModifier.CMD);
        screen.type(",", KeyModifier.CMD);

        Util.clickWithPattern("extensions", 0.80f);
        if (screen.exists("extnTabsFolder", (double) 10) != null) {
            Util.clickWithPattern("extnTabsFolder", 0.80f);
        } else {
            Util.clickWithPattern("extnTab2", 0.80f);
        }
        Util.click("uninstall");
        Util.click("uninstall2");

        screen.wait((double) 3.0);

        Assert.assertNull(screen.exists("extnTabsFolder"));
        Assert.assertNull(screen.exists("extnTab2"));
        screen.type("w", KeyModifier.CMD);
    }

    @After
    public void cleanup() throws FindFailed {

    }
}
