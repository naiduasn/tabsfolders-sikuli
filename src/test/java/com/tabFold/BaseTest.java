package com.tabFold;

import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.sikuli.basics.Debug;
import org.sikuli.script.App;
import org.sikuli.script.ImagePath;
import org.sikuli.script.Key;
import org.sikuli.script.Screen;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by sanyasi.annepu on 21/08/15.
 */
public class BaseTest {

    App sfr;

    @Rule
    public TestName name = new TestName();
    @Rule
    public TestRule testWatcher = new TestWatcher() {

        @Override
        public void failed(Throwable e, Description d) {
            Debug.info("Creating screenshot...");
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            Rectangle screenRectangle = new Rectangle(screenSize);
            try {
                Robot robot = new Robot();
                BufferedImage image = robot.createScreenCapture(screenRectangle);
                ImageIO.write(image, "png", new File(name.getMethodName() + "_failed.png"));
            } catch (Exception ex) {
                Debug.error("Failed to create a screenshot");
            }
        }

        @Override
    public void starting(Description d){
            Debug.info("Starting test case...");
        }

        @Override
        public void finished(Description d){
            Debug.info("Completing the test...");
            try {
                Screen screen = new Screen();
                Util.closeTabs();
                screen.wait((double) 2.0);
                if (name.getMethodName().equalsIgnoreCase("test4MultipleTabSave") || name.getMethodName().equalsIgnoreCase("test6MoveBookmarks") || name.getMethodName().equalsIgnoreCase("testUnInstallPlugin")) {
                    screen.type("q", Key.CMD);
                } else {
                    sfr.close();
                }
                ImagePath.reset();
                screen.wait((double) 5.0);
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }
    };


}

